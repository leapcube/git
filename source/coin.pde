public class coin{
  float xCoin, yCoin;
  int coinHeight, coinWidth;
  
  coin(int j){
    xCoin = (j*160);
    yCoin= (j*random(200, 550));
    coinWidth = 30;
    coinHeight = 30;
 }
 
 void drawCoin(){
    stroke(246, 215, 15);
    fill(246, 215, 15);
    strokeWeight(2);
    rect(xCoin, yCoin ,coinWidth,coinHeight);
    fill(229, 200, 17);
    rect(xCoin + 7,yCoin + 7, coinWidth - 13, coinHeight - 13);
 }
 
 void checkPosition(){
  if(xCoin<0){
   xCoin+=(160*8);
   yCoin = random(100, 650);
  } 
 }
}
