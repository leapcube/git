public class rotationWall {
  float xRotateWall, yRotateWall, angle, xSpeedRotateWall;  
  
  rotationWall(){
  xRotateWall = 1250;
  yRotateWall = 300;
  xSpeedRotateWall = -3;
  angle = 0;
  }
     
  void drawRotationWall(){
    pushMatrix(); 
    translate(xRotateWall, yRotateWall);
    rotate(angle);
    stroke(255);
    strokeWeight(20); 
    rotate(angle); 
    line(-55, 0, 55, 0);
    line(0, 55, 0, -55);
    angle -= 0.04;
    popMatrix();
    
  }
  
  void move(){
    xRotateWall += xSpeedRotateWall;
  }
  
  void checkPosition(){

  if(xRotateWall<0){
    xRotateWall = 1250;
  }
 }
}
 
  
